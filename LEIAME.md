# Projeto asl-cursos #

Este projeto consiste em:

Uma imagem para construção de um pen drive bootável.

Essa imagem contém um sistema debian, bullseye.
Nela é contido também:

O sistema básico de ambiente de texto padrão de sistemas linux.

Alguns aplicativos para funcionamento em rede.

Sistema de som com alsa e pulseaudio.

**Sistema de estudos white-new**.

O projeto white-new, consiste em um aplicativos escrito em PERL.
Este usando também o ambiente de janelas curses, provê:
10 estudos dirigidos, na forma de roteiros; em cada uma das seguintes disciplinas.

* Matemática.

* Eletricidade.

* Sistemas operacionais.

* Tecnologia da infromação.

O sistema white-new contido na iso, disponibiliza o aplicativo e os estudos em três idiomas.

- Inglês.

- Espanhol.

- Português.

A idéia do projeto é direcionar e ministrar estudos dentro dessas áreas.
Todos os conteúdo tem relacção com a ciência da computação.

## Construindo o pen drive ##

Clonar o repositorio.

~:git clone https://codeberg.org/Linux77/asl-cursos_live

~: cd asl-curso_live

dd status=progress if=live-image-amd64.img of /dev/sdX 

> (onde X) representa o dispositivo pen drive.

Após isso basta iniciar o computador onde se pretende executar o sistema.

Após a inicialização, no diretório do usuário padão do sistema recém inicializado serão encontrados, três scripts.

português
espanhol
english

Para executar o sistema white-new, que contém os estudos basta:

~/:./português
> Para execução do sistema em português.

~:./espanhol
> Para execução do sistema em espanhol.

~:./english
> Para execução do sistema em ingles.

O sistema possui uma interface amigável, e permite a leitura dos textos ou ainda sua audição.
A navegação pelos menus do sistema se faz com a tecla "tab", a seleção com a tecla "espaço".

> A opção de ouvir, permite ouvir os textos no idioma que está sendo executado o sistema.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
