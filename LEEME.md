# proyecto de cursos-asl #

Este proyecto consta de:

Una imagen para crear un pendrive de arranque.

Esta imagen contiene un sistema Debian, bullseye.
También contiene:

El sistema de entorno de texto estándar básico para sistemas Linux.

Algunas aplicaciones para la creación de redes.

Sistema de sonido con alsa y pulseaudio.

** Sistema de estudio blanco nuevo **.

El proyecto white-new consiste en una aplicación escrita en PERL.
Esto también usando el entorno de ventanas curses, proporciona:
10 estudios dirigidos, en forma de guiones; en cada uno de los siguientes temas.

* Matemáticas.

* Electricidad.

* Sistemas operacionales.

* Tecnologías de la información.

El sistema white-new contenido en iso hace que la aplicación y los estudios estén disponibles en tres idiomas.

- Inglés.

- Español.

- portugués.

La idea del proyecto es dirigir y realizar estudios dentro de estas áreas.
Todo el contenido está relacionado con la informática.

## Construyendo el pendrive ##

Clona el repositorio.

~: clon de git https://codeberg.org/Linux77/asl-curso_live

~: cd asl-curso_live
dd status = progreso if = live-image-amd64.img of / dev / sdX

> (donde X) representa el dispositivo pen drive.

Después de eso, simplemente inicie la computadora donde desea ejecutar el sistema.

Después de arrancar, en el directorio de usuario predeterminado del sistema recién arrancado, se encontrarán tres scripts.

portugués
Español
inglés

Para ejecutar el sistema white-new, que contiene los estudios, simplemente:

~ /:./ inglés
> Para ejecutar el sistema en portugués.

~: ./ Español
> Ejecutar el sistema en español.

~: ./ inglés
> Para ejecutar el sistema en inglés.

El sistema tiene una interfaz fácil de usar y permite leer los textos o incluso escucharlos.
La navegación por los menús del sistema se realiza con la tecla "tab", la selección con la tecla "espacio".

> La opción de escuchar, te permite escuchar los textos en el idioma en el que se está ejecutando el sistema.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
