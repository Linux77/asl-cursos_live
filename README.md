# asl-courses project #

---

[Versión en español de este documento](LEEME.md).

[Versão em português deste documento](LEIAME.md).

This project consists of:

An image for building a bootable pen drive.

This image contains a debian system, bullseye.
It also contains:

The basic standard text environment system for linux systems.

Some applications for networking.

Sound system with alsa and pulseaudio.

**White-new study system**.

The white-new project consists of an application written in PERL.
This also using the curses windowing environment, provides:
10 directed studies, in the form of scripts; in each of the following subjects.

* Math.

* Electricity.

* Operational systems.

* Information technology.

The white-new system contained in iso makes the application and studies available in three languages.

- English.

- Spanish.

- Portuguese.

The project's idea is to direct and conduct studies within these areas.
All content is computer science related.

## Building the pen drive ##

Clone the repository.

~:git clone https://codeberg.org/Linux77/asl-cursos_live

~: cd asl-curso_live
dd status=progress if=live-image-amd64.img of /dev/sdX

> (where X) represents the pen drive device.

After that, just start the computer where you want to run the system.

After booting, in the default user directory of the newly booted system, three scripts will be found.

Portuguese
Spanish
english

To run the white-new system, which contains the studies, just:

~/:./english
> To run the system in Portuguese.

~:./Spanish
> To run the system in Spanish.

~:./english
> To run the system in English.

The system has a user-friendly interface, and allows reading the texts or even hearing them.
Navigation through the system menus is done with the "tab" key, selection with the "space" key.

> The option to listen, allows you to hear the texts in the language that the system is running.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
